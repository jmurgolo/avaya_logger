USE [avayalog]
GO

/****** Object:  Table [dbo].[Event]    Script Date: 04/25/2014 15:13:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Event](
	[idnew_table] [int] IDENTITY(95762,1) NOT NULL,
	[time_stamp] [datetime] NOT NULL,
	[call_start_time] [datetime] NULL,
	[connected_time] [varchar](50) NULL,
	[ring_time] [int] NULL,
	[caller] [varchar](200) NULL,
	[Direction] [varchar](1) NULL,
	[called_number] [varchar](500) NULL,
	[dialed_number] [varchar](500) NULL,
	[account] [varchar](45) NULL,
	[is_internal] [varchar](1) NULL,
	[call_id] [varchar](45) NULL,
	[continuation] [varchar](1) NULL,
	[party1device] [varchar](500) NULL,
	[party1name] [varchar](500) NULL,
	[party2device] [varchar](500) NULL,
	[party2name] [varchar](500) NULL,
	[hold_time] [real] NULL,
	[park_time] [real] NULL,
	[authvalid] [varchar](1) NULL,
	[authcode] [varchar](500) NULL,
	[user_charged] [varchar](500) NULL,
	[call_charged] [varchar](50) NULL,
	[currency] [varchar](45) NULL,
	[amount_at_last_user_change] [varchar](500) NULL,
	[call_units] [varchar](100) NULL,
	[units_at_last_user_change] [varchar](500) NULL,
	[cost_per_unit] [varchar](50) NULL,
	[mark_up] [varchar](50) NULL,
	[external_targeting_cause] [varchar](100) NULL,
	[external_targeter_id] [varchar](500) NULL,
	[external_targetted_number] [varchar](50) NULL,
	[source_ip_address] [varchar](45) NULL,
	[connected_time_temp] [varchar](20) NULL,
	[call_start_time_old] [datetime2](0) NULL,
 CONSTRAINT [PK_Event_New_idnew_table] PRIMARY KEY CLUSTERED 
(
	[idnew_table] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'avayalog.Event_New' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Event'
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (getdate()) FOR [time_stamp]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [connected_time]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [ring_time]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [caller]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [Direction]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [called_number]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [dialed_number]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [account]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [is_internal]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [call_id]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [continuation]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [party1device]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [party1name]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [party2device]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [party2name]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [hold_time]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [park_time]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [authvalid]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [authcode]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [user_charged]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [call_charged]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [currency]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [amount_at_last_user_change]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [call_units]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [units_at_last_user_change]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [cost_per_unit]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [mark_up]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [external_targeting_cause]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [external_targeter_id]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [external_targetted_number]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [source_ip_address]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [connected_time_temp]
GO

ALTER TABLE [dbo].[Event] ADD  DEFAULT (NULL) FOR [call_start_time_old]
GO


