/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AvayaLogger;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import Database.CallBean;
import Database.CallJDBCDAO;
import Email.Email;

/**
 *
 * @author jmm
 */

public class AvayaLogger implements Runnable {
         
		private static int port = 9000;
        private Socket connection;
        private int ID;
        private static int count = 0;             
        protected static final Logger logger = Logger.getLogger(AvayaLogger.class);
        
        AvayaLogger(Socket socket, int id) {
                this.connection = socket;
                this.ID = id;
        }        
        
        private static boolean stop = false;
        
        public static void main(String[] args) throws AddressException, MessagingException  {
        
        	if(args.length > 0){
        	if ("start".equals(args[0]))  {
                start(args);
            } else if ("stop".equals(args[0])) {
                stop(args);
            } 
        	} else {
            	start(args);
            }     		
	}
    
    public static void start(String[] args) throws AddressException, MessagingException {
        Email.sendEmail("Avay Logger Started", "test"); 
        String resource = "log4j.properties";
        URL configFileResource = AvayaLogger.class.getResource(resource);
        PropertyConfigurator.configure(configFileResource);
        ServerSocket socket = null;
        logger.debug("Application Started");
        //System.out.println("Application Started");
        
        try {
            socket = new ServerSocket(port);
            System.out.println("SocketServer Initialized");

            while (!stop) {
                Socket connection = socket.accept();
                Runnable runnable = new AvayaLogger(connection, ++count);
                Thread thread = new Thread(runnable);
                thread.start();
            }                                        
        } catch (Exception e) {
            logger.error("Xception:",e);
            Email.sendEmail("Error - Avaya Logger", "Error: " + e);
        } finally {
        	if(socket != null){
        		try {
        			System.out.println("listening3");
					socket.close();
				} catch (IOException e) {
					logger.error("Xception:",e);
					Email.sendEmail("Error - Avaya Logger", "Error: " + e);
				}
        	}

        }
    }

@Override
public void run() {
    
	logger.setLevel(Level.ERROR);
    String inetAddressString =  connection.getRemoteSocketAddress().toString();
    logger.debug("count:"+ ID + " New thread started from: " + inetAddressString);
    String line = "";
    String bufferContents = "";
    BufferedInputStream is = null;
    InputStreamReader isr = null;
    BufferedReader br = null; 
    CallJDBCDAO callDatabaseConn = new CallJDBCDAO() ; 
    CallBean callBean = null;
        
        
	try {
			is = new BufferedInputStream(connection.getInputStream());
			isr = new InputStreamReader(is);
            br = new BufferedReader(isr);
            
            while ((line = br.readLine()) != null){
                bufferContents = line + "," + inetAddressString;
                if((bufferContents.split(",").length) != 31){
                    try {
        				logger.error("Database Insert Exeption - incorrect string format: length = " + (bufferContents.split(",").length) + " " + bufferContents);
        				Email.sendEmail("Error - Avaya Logger", "Insert Error = incorrect string format: length = " + (bufferContents.split(",").length) + " " + bufferContents);
        			} catch (AddressException e1) {
        				logger.error("Xception:",e1);
        			} catch (MessagingException e1) {
        				logger.error("Xception:",e1);
        			}
                }else{
                    callBean = new CallBean(bufferContents);
                	callDatabaseConn.add(callBean.getCallBean());   
    				logger.info("Insert into Database Successful: " + (bufferContents.split(",").length) + " " + bufferContents);
                }
            }                  
            logger.info("End of Transmission");                        
	} catch (IOException e) {
		try {
			logger.error("Xception:",e);
			Email.sendEmail("Error - Avaya Logger", "Error: " + e);
		} catch (AddressException e1) {
			logger.error("Xception:",e1);
		} catch (MessagingException e1) {
			logger.error("Xception:",e1);
		}
	} catch (Exception e) {
		try {
			logger.error("Xception:",e);
			Email.sendEmail("Error - Avaya Logger", "Error: " + e);
		} catch (AddressException e1) {
			logger.error("Xception:",e1);
		} catch (MessagingException e1) {
			logger.error("Xception:",e1);
		}
	} finally {
		try {        
            logger.debug("Connection Closed");
            connection.close(); 
            br.close();
            isr.close();
            is.close();
            callBean = null;
            callDatabaseConn = null;
		} catch (IOException e) {
			try {
				logger.error("Xception:",e);
				Email.sendEmail("Error - Avaya Logger", "Error: " + e);
			} catch (AddressException e1) {
				logger.error("Xception:",e1);
			} catch (MessagingException e1) {
				logger.error("Xception:",e1);
			}
		}
	}
    }
    
    public static void stop(String[] args) {
        stop = true;
    }

}