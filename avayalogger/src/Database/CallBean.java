/**
 * 
 */
package Database;

/**
 * @author jmm
 *
 */

import java.io.Serializable;

public class CallBean implements Serializable  {
	
	private static final long serialVersionUID = 1L;
	private String[] CallString;
	
	public CallBean() {
		
	}
	
	public CallBean(String s) {
		this.CallString = s.split(",");
	}	
	
	public String[] getCallBean(){
		return CallString;
	}
}
