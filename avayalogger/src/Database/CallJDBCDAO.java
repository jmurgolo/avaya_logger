/**
 * 
 */
package Database;

/**
 * @author jmm
 *
 */

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.log4j.Logger;

import Email.Email;

public class CallJDBCDAO {
        
	Connection connection = null;
    PreparedStatement ptmt = null;
    ResultSet resultSet = null;

    private static final Logger logger = Logger.getLogger(CallJDBCDAO.class);
    String resource = "log4j.properties";
    URL configFileResource = CallJDBCDAO.class.getResource(resource);
    
    public CallJDBCDAO() {

    }

    private Connection getConnection() throws SQLException {
            Connection conn;
            conn = ConnectionFactory.getInstance().getConnection();
            return conn;
    }

    public void add(String[] splitInputString) {
    	
    	String sqlInsert = "";
    	
    	try {
		    sqlInsert  = "insert into Event (call_start_time,connected_time,ring_time,caller,Direction,called_number,dialed_number,account,is_internal,call_id,continuation,party1device,party1name,party2device,party2name,hold_time,park_time,authvalid,authcode,user_charged,call_charged,currency,amount_at_last_user_change,call_units,units_at_last_user_change,cost_per_unit,mark_up,external_targeting_cause,external_targeter_id,external_targetted_number,source_ip_address)" 
			            
		    + "values ('" + splitInputString[0] 
		    + "','" + splitInputString[1]
		    + "','" + splitInputString[2]
		    + "','" + splitInputString[3]
		    + "','" + splitInputString[4]
		    + "','" + splitInputString[5]
		    + "','" + splitInputString[6]
			+ "','" + splitInputString[7]
			+ "','" + splitInputString[8]
			+ "','" + splitInputString[9]
			+ "','" + splitInputString[10]
			+ "','" + splitInputString[11]
			+ "','" + splitInputString[12]
			+ "','" + splitInputString[13]
			+ "','" + splitInputString[14]
			+ "','" + splitInputString[15]
			+ "','" + splitInputString[16]
			+ "','" + splitInputString[17]
			+ "','" + splitInputString[18]
			+ "','" + splitInputString[19]
			+ "','" + splitInputString[20]
		    + "','" + splitInputString[21]
			+ "','" + splitInputString[22]
			+ "','" + splitInputString[23]
			+ "','" + splitInputString[24]
			+ "','" + splitInputString[25]
			+ "','" + splitInputString[26]
			+ "','" + splitInputString[27]
			+ "','" + splitInputString[28]
			+ "','" + splitInputString[29]
			+ "','" + splitInputString[30] +"')";
			connection = getConnection();
		    ptmt = connection.prepareStatement(sqlInsert);
			ptmt.executeUpdate();			
		} catch (SQLException e) {
			try {
				Email.sendEmail("Error - Avaya Logger", "Error: " + e);
				logger.error("Insert Error = " + e + " with " + sqlInsert );
			} catch (AddressException e1) {
				logger.error("Insert Error = " + e );
				logger.error("Insert Error = " + e1 );

			} catch (MessagingException e1) {
				logger.error("Insert Error = " + e );
				logger.error("Insert Error = " + e1 );
			}			
		} finally {
			try {
		        if (ptmt != null)
		                ptmt.close();
		        if (connection != null)
		                connection.close();
			} catch (SQLException e) {
				try {
					Email.sendEmail("Error - Avaya Logger", "Error: " + e);
					logger.error("Insert Error = " + e );
				} catch (AddressException e1) {
					logger.error("Insert Error = " + e );
					logger.error("Insert Error = " + e1 );

				} catch (MessagingException e1) {
					logger.error("Insert Error = " + e );
					logger.error("Insert Error = " + e1 );
				}
			} catch (Exception e) {
                try {
					Email.sendEmail("Error - Avaya Logger", "Error: " + e);
					logger.error("Insert Error = " + e );
				} catch (AddressException e1) {
					logger.error("Insert Error = " + e );
					logger.error("Insert Error = " + e1 );

				} catch (MessagingException e1) {
					logger.error("Insert Error = " + e );
					logger.error("Insert Error = " + e1 );
				}
			}
	
	}

    }

      
}