/**
 * 
 */
package Database;

/**
 * @author jmm
 *
 */


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import Email.Email;

public class ConnectionFactory {

    String driverClassName = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    String connectionUrl = "jdbc:sqlserver://localhost;instanceName=sqlexpress;databaseName=avayalog";
    String dbUser = "";
    String dbPwd = "";

    private static ConnectionFactory connectionFactory = null;

    private ConnectionFactory() {
            try {
                    Class.forName(driverClassName);
            } catch (ClassNotFoundException e) {
                try {
					Email.sendEmail("Error - Avaya Logger", "Error: " + e);
				} catch (AddressException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					e.printStackTrace();
				} catch (MessagingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					e.printStackTrace();
				}                    
            }
    }

    public Connection getConnection() throws SQLException {
            Connection conn = null;
            conn = DriverManager.getConnection(connectionUrl, dbUser, dbPwd);
            return conn;
    }

    public static ConnectionFactory getInstance() {
            if (connectionFactory == null) {
                    connectionFactory = new ConnectionFactory();
            }
            return connectionFactory;
    }
}
