/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Email;

/**
 *
 * @author jmm
 */

import java.util.Date;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;

public class Email {
	
    protected static String host = "st.sp.sw";
    protected static String port = "25";
    protected static String userName = "userid";
    protected static String password = "password";
    protected static String toAddress = "someone@someplace.somewhere";
    
    public static void sendEmail(String subject, String message)
            throws AddressException, MessagingException {
       
        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        //properties.put("mail.smtp.auth", "true");
       // properties.put("mail.smtp.starttls.enable", "false");
        //properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.user", userName);
        properties.put("mail.password", password);
        
        //properties.put("mail.smtp.socketFactory.port", port);
        //properties.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
        
                // creates a new session with an authenticator
        Authenticator auth = new Authenticator() {

            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        };
        Session session = Session.getInstance(properties, auth);

        // creates a new e-mail message
        Message msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress("Avayalogger@springfieldcityhall.com"));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject);
        msg.setSentDate(new Date());  

        // creates message part
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(message, "text/html");

        // creates multi-part
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);

        // sets the multi-part as e-mail's content
        msg.setContent(multipart);

        // sends the e-mail
        Transport.send(msg);

    }
}